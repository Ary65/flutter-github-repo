class AssetConstants {
  static const nameIcon = 'assets/name.svg';
  static const ownerIcon = 'assets/owner.svg';
  static const lastUpdateIcon = 'assets/last-updated.svg';
  static const descriptionIcon = 'assets/desc.svg';
  static const arrowRight = 'assets/fi-rr-arrow-right.svg';
  static const arrowBack = 'assets/fi-rr-arrow-left.svg';
}
