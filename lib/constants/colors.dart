import 'package:flutter/material.dart';

class AppColors {
  static const primaryColor = Color(0xff3C79F5);
  static const secondaryColor = Color(0xff0046D5);
  static const opacityColor = Color(0xffD9D9D9);
  static const whiteColor = Color(0xfffdffff);
  //* Gradient
  static const gradient = LinearGradient(
    begin: Alignment.centerRight,
    end: Alignment.centerLeft,
    colors: [
      Color(0xfff4f8ff),
      Color(0xffe9f0fd),
    ],
  );
}
