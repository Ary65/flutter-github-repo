import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';

class ConnectivityService {
  static late BuildContext _context;
  static late OverlayEntry _overlayEntry;
  static late bool _isConnected;

  static void initialize(BuildContext context) {
    _context = context;
    _isConnected = true; // Assuming internet is initially connected
    _initializeConnectivityListener();
  }

  static void _initializeConnectivityListener() {
    Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      if (result == ConnectivityResult.none) {
        _showNoInternetSnackbar();
        _isConnected = false;
      } else {
        if (!_isConnected) {
          _showConnectionRestoredSnackbar();
          _isConnected = true;
        }
      }
    });
  }

  static void _showNoInternetSnackbar() {
    const snackBar = SnackBar(
      content: Text('No internet connection'),
      duration: Duration(seconds: 3),
    );
    ScaffoldMessenger.of(_context).showSnackBar(snackBar);
  }

  static void _showConnectionRestoredSnackbar() {
    const snackBar = SnackBar(
      content: Text('Your internet connection was restored'),
      duration: Duration(seconds: 3),
    );
    ScaffoldMessenger.of(_context).showSnackBar(snackBar);
  }
}
