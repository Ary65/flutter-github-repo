import 'package:fluttter_github_repo/models/repository_model.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseService {
  static Future<Database> openDatabaseConnection() async {
    return openDatabase(
      join(await getDatabasesPath(), 'repositories.db'),
      onCreate: (db, version) {
        return db.execute(
          'CREATE TABLE repositories(id INTEGER PRIMARY KEY, name TEXT, owner TEXT, avatar_url TEXT,description TEXT, lastUpdated TEXT,stargazers_count INTEGER)',
        );
      },
      version: 1,
    );
  }

  static Future<void> saveRepositories(
      List<RepositoryModel> repositories) async {
    final database = await openDatabaseConnection();
    for (var repo in repositories) {
      await database.insert(
        'repositories',
        repo.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace,
      );
    }
  }

  static Future<List<RepositoryModel>> loadRepositoriesFromDatabase() async {
    final database = await openDatabaseConnection();
    final List<Map<String, dynamic>> maps =
        await database.query('repositories');
    return List.generate(maps.length, (i) {
      return RepositoryModel(
        name: maps[i]['name'],
        owner: maps[i]['owner'],
        ownerAvatarUrl: maps[i]['avatar_url'],
        description: maps[i]['description'],
        lastUpdated: maps[i]['lastUpdated'],
        stargazersCount: maps[i]['stargazers_count'] ?? 0,
      );
    });
  }
}
