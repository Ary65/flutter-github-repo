import 'dart:async';
import 'package:dio/dio.dart';
import 'package:dio_smart_retry/dio_smart_retry.dart';
import 'package:flutter/material.dart';
import 'package:fluttter_github_repo/models/repository_model.dart';

class ApiService {
  static Future<List<RepositoryModel>> fetchRepositories(int page) async {
    final dio = _getDioWithInterceptor();
    try {
      final response = await dio.get(
        'https://api.github.com/search/repositories',
        queryParameters: {
          'q': 'flutter',
          'per_page': 10,
          'page': page,
        },
      );
      if (response.statusCode == 200) {
        final List<RepositoryModel> repos = [];
        for (var item in response.data['items']) {
          repos.add(RepositoryModel.fromJson(item));
        }
        return repos;
      } else {
        throw Exception('Failed to load repositories');
      }
    } catch (e) {
      debugPrint('Error fetching repositories: $e');
      rethrow;
    }
  }

  static _getDioWithInterceptor() {
    final dio = Dio();
    dio.interceptors.add(RetryInterceptor(
      dio: dio,
      logPrint: print,
      retries: 3,
      retryDelays: const [
        Duration(seconds: 1),
        Duration(seconds: 2),
        Duration(seconds: 3),
      ],
    ));
    return dio;
  }

  static void scheduleApiCall() {
    Timer.periodic(const Duration(minutes: 30), (timer) async {
      try {
        await fetchRepositories(1);

        debugPrint('Repositories fetched at ${DateTime.now()}');
      } catch (e) {
        debugPrint('Failed to fetch repositories: $e');
      }
    });
  }
}
