import 'package:shared_preferences/shared_preferences.dart';

class PreferencesService {
  static Future<String> loadSortingOption() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('sort_option') ?? 'Last Updated';
  }

  static Future<void> saveSortingOption(String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('sort_option', value);
  }
}