import 'package:flutter/material.dart';
import 'package:fluttter_github_repo/models/repository_model.dart';
import 'package:fluttter_github_repo/pages/repository_details_page.dart';
import 'package:fluttter_github_repo/pages/repository_list_page.dart';
import 'package:go_router/go_router.dart';

//*
CustomTransitionPage<T> buildPageWithSlideTransition<T>({
  required BuildContext context,
  required GoRouterState state,
  required Widget child,
}) {
  return CustomTransitionPage<T>(
    transitionDuration: const Duration(milliseconds: 200),
    key: state.pageKey,
    child: child,
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      const begin = Offset(1.0, 0.0); // Start off screen to the right
      const end = Offset.zero; // Slide in to the left
      const curve = Curves.easeInOut;

      var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

      var slideAnimation = animation.drive(Tween(begin: begin, end: end));

      return SlideTransition(
        position: slideAnimation,
        child: child,
      );
    },
  );
}

final GoRouter router = GoRouter(routes: <RouteBase>[
  GoRoute(
    path: '/',
    builder: (context, state) => const RepositoryListPage(),
    pageBuilder: (context, state) => buildPageWithSlideTransition<void>(
      context: context,
      state: state,
      child: const RepositoryListPage(),
    ),
    routes: <RouteBase>[
      GoRoute(
          path: 'details',
          builder: (context, state) {
            final repository = state.extra as RepositoryModel;
            return RepositoryDetailsPage(repository: repository);
          },
          pageBuilder: (context, state) {
            final repository = state.extra as RepositoryModel;
            return buildPageWithSlideTransition<void>(
              context: context,
              state: state,
              child: RepositoryDetailsPage(repository: repository),
            );
          }),
    ],
  )
]);
