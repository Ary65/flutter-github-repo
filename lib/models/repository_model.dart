class RepositoryModel {
  final String name;
  final String owner;
  final String ownerAvatarUrl;
  final String description;
  final String lastUpdated;
  final int stargazersCount;

  RepositoryModel({
    required this.name,
    required this.owner,
    required this.ownerAvatarUrl,
    required this.description,
    required this.lastUpdated,
    required this.stargazersCount,
  });

  factory RepositoryModel.fromJson(Map<String, dynamic> json) {
    return RepositoryModel(
      name: json['name'],
      owner: json['owner']['login'],
      ownerAvatarUrl: json['owner']['avatar_url'],
      description: json['description'] ?? 'No description available',
      lastUpdated: json['updated_at'],
      stargazersCount: json['stargazers_count'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'owner': owner,
      'avatar_url': ownerAvatarUrl,
      'description': description,
      'lastUpdated': lastUpdated,
      'stargazers_count': stargazersCount,
    };
  }
}
