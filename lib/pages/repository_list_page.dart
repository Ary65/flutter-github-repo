import 'dart:async';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:fluttter_github_repo/models/repository_model.dart';
import 'package:fluttter_github_repo/services/api_service.dart';
import 'package:fluttter_github_repo/services/connectivity_service.dart';
import 'package:fluttter_github_repo/services/database_service.dart';
import 'package:fluttter_github_repo/services/prefernces_service.dart';
import 'package:fluttter_github_repo/widgets/reository_list_appbar.dart';
import 'package:fluttter_github_repo/widgets/offline_body.dart';
import 'package:fluttter_github_repo/widgets/online_body.dart';

class RepositoryListPage extends StatefulWidget {
  const RepositoryListPage({super.key});

  @override
  State<RepositoryListPage> createState() => _RepositoryListPageState();
}

class _RepositoryListPageState extends State<RepositoryListPage> {
  List<RepositoryModel> repositories = []; // Initialize as an empty list
  final ScrollController _scrollController = ScrollController();
  bool _isLoading = false;
  bool _isOffline = false;
  int _page = 1;
  String _sortBy = 'Last Updated'; // Default sorting option

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_scrollListener);
    _initializeConnectivityListener();
    ConnectivityService.initialize(context);
    ApiService.scheduleApiCall();
    PreferencesService.loadSortingOption().then((value) {
      setState(() {
        _sortBy = value;
      });
    });
  }

  void _initializeConnectivityListener() {
    Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      setState(() {
        _isOffline = result == ConnectivityResult.none;
        if (!_isOffline) {
          fetchRepositories();
        } else {
          _loadDataFromLocalDatabase();
        }
      });
    });
  }

  Future<void> fetchRepositories() async {
    if (_isLoading) return;
    setState(() {
      _isLoading = true;
    });

    try {
      final repos = await ApiService.fetchRepositories(_page);
      setState(() {
        if (_page == 1) {
          repositories = repos;
        } else {
          repositories.addAll(repos);
        }
        _isLoading = false;
      });
      // Save repositories to local database
      await DatabaseService.saveRepositories(repos);
    } catch (e) {
      setState(() {
        _isLoading = false;
      });
      // Handle error
    }
  }

  Future<void> _loadDataFromLocalDatabase() async {
    try {
      final cachedRepositories =
          await DatabaseService.loadRepositoriesFromDatabase();
      setState(() {
        repositories = cachedRepositories;
      });
    } catch (e) {
      // Handle error
    }
  }

  void _scrollListener() {
    if (_scrollController.offset >=
            _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange) {
      _page++;
      fetchRepositories();
    }
  }

  void _onSortByChanged(String? value) async {
    await PreferencesService.saveSortingOption(value!);

    setState(() {
      _sortBy = value;
      // Sort repositories based on selected option
      if (_sortBy == 'Last Updated') {
        repositories.sort((a, b) => a.lastUpdated.compareTo(b.lastUpdated));
      } else if (_sortBy == 'Star Count') {
        repositories
            .sort((a, b) => b.stargazersCount.compareTo(a.stargazersCount));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: RepositoryListAppBar(
        sortBy: _sortBy,
        onSortByChanged: _onSortByChanged,
      ),
      body: _isOffline
          ? const OfflineBody()
          : OnlineBody(
              scrollController: _scrollController,
              repositories: repositories,
              isLoading: _isLoading,
            ),
    );
  }
}
