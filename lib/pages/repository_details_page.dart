import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:fluttter_github_repo/constants/asset_constans.dart';
import 'package:fluttter_github_repo/constants/colors.dart';
import 'package:fluttter_github_repo/models/repository_model.dart';
import 'package:fluttter_github_repo/widgets/details_appbar.dart';
import 'package:fluttter_github_repo/widgets/profile_card.dart';
import 'package:intl/intl.dart';

class RepositoryDetailsPage extends StatelessWidget {
  final RepositoryModel repository;

  const RepositoryDetailsPage({super.key, required this.repository});

  @override
  Widget build(BuildContext context) {
    // Format the DateTime object
    String dateStringWithTimeZone = repository.lastUpdated;
    String formattedLastUpdated = DateFormat('MM-dd-yyyy-HH:mm:ss')
        .format(DateTime.parse(dateStringWithTimeZone));
    return Scaffold(
      appBar: const DetailsAppBar(),
      body: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          gradient: AppColors.gradient,
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 24),
              Center(
                child: CircleAvatar(
                  radius: 80,
                  backgroundImage:
                      CachedNetworkImageProvider(repository.ownerAvatarUrl),
                ),
              ),
              const SizedBox(height: 16),
              ProfileCard(
                title: 'Name',
                subTitle: repository.name,
                svgIconPath: AssetConstants.nameIcon,
              ),
              ProfileCard(
                title: 'Owner',
                subTitle: repository.owner,
                svgIconPath: AssetConstants.ownerIcon,
              ),
              ProfileCard(
                title: 'Last Updated',
                subTitle: formattedLastUpdated,
                svgIconPath: AssetConstants.lastUpdateIcon,
              ),
              ProfileCard(
                title: 'Descriptions',
                subTitle: repository.description,
                svgIconPath: AssetConstants.descriptionIcon,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
