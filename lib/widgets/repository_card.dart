import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttter_github_repo/constants/asset_constans.dart';
import 'package:fluttter_github_repo/constants/colors.dart';
import 'package:fluttter_github_repo/models/repository_model.dart';
import 'package:go_router/go_router.dart';

class RepositoryCard extends StatelessWidget {
  const RepositoryCard({
    super.key,
    required this.repository,
  });

  final RepositoryModel repository;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      child: Ink(
        height: 96,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          gradient: AppColors.gradient,
        ),
        child: ListTile(
          splashColor: AppColors.primaryColor.withOpacity(.5),

          title: Text(repository.name,
              style: const TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w700,
                  color: AppColors.primaryColor)), // Bold repository name
          subtitle: Text(repository.description,
              maxLines: 2,
              style: const TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w500,
              )),
          trailing: Card(
            color: AppColors.primaryColor,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: SvgPicture.asset(AssetConstants.arrowRight),
            ),
          ),
          onTap: () => context.push(
            '/details',
            extra: repository,
          ),
        ),
      ),
    );
  }
}
