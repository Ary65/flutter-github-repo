import 'package:flutter/material.dart';
import 'package:fluttter_github_repo/models/repository_model.dart';
import 'package:fluttter_github_repo/widgets/repository_card.dart';

class OnlineBody extends StatelessWidget {
  final ScrollController scrollController;
  final List<RepositoryModel> repositories;
  final bool isLoading;

  const OnlineBody({
    super.key,
    required this.scrollController,
    required this.repositories,
    required this.isLoading,
  });

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        ListView.builder(
          controller: scrollController,
          itemCount: repositories.length,
          itemBuilder: (context, index) {
            if (index == repositories.length) {
              return isLoading
                  ? const Center(child: CircularProgressIndicator())
                  : const SizedBox();
            }
            final repository = repositories[index];
            return RepositoryCard(repository: repository);
          },
        ),
        if (isLoading && repositories.isNotEmpty)
          const Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: LinearProgressIndicator(),
          ),
      ],
    );
  }
}
