// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttter_github_repo/constants/colors.dart';

 

class ProfileCard extends StatelessWidget {
  final String title;
  final String subTitle;
  final String svgIconPath;
  const ProfileCard(
      {super.key,
      required this.title,
      required this.subTitle,
      required this.svgIconPath});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Card(
        color: AppColors.primaryColor,
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: SvgPicture.asset(svgIconPath),
        ),
      ),
      title: Text(title,
          style: const TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w600,
          )),
      subtitle: Text(subTitle,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: const TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w700,
          )),
    );
  }
}
