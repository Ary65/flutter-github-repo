import 'package:flutter/material.dart';
import 'package:fluttter_github_repo/constants/colors.dart';


class RepositoryListAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String sortBy;
  final Function(String?) onSortByChanged;

  const RepositoryListAppBar({
    super.key,
    required this.sortBy,
    required this.onSortByChanged,
  });

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight + 16);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: const Text(
        "Flutter GitHub Repo.",
        style: TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.w700,
          color: Colors.white,
        ),
      ),
      backgroundColor: AppColors.primaryColor, // Set custom background color
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(20),
          bottomRight: Radius.circular(20),
        ),
      ),
      actions: [
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 12),
          margin: const EdgeInsets.only(right: 12),
          decoration: BoxDecoration(
            color: AppColors.secondaryColor,
            borderRadius: BorderRadius.circular(16),
          ),
          child: DropdownButton<String>(
            underline: const SizedBox(),
            dropdownColor: AppColors.secondaryColor,
            style: const TextStyle(
              color: Colors.white,
            ),
            icon: const Icon(
              Icons.arrow_drop_down,
              color: Colors.white,
            ),
            value: sortBy,
            onChanged: onSortByChanged,
            items: <String>['Last Updated', 'Star Count']
                .map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }).toList(),
          ),
        ),
      ],
    );
  }
}
