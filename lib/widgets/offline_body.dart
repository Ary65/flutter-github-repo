import 'package:flutter/material.dart';
import 'package:fluttter_github_repo/models/repository_model.dart';
import 'package:fluttter_github_repo/services/database_service.dart';
import 'package:fluttter_github_repo/widgets/repository_card.dart'; 

class OfflineBody extends StatelessWidget {
  const OfflineBody({super.key});

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<RepositoryModel>>(
      future: DatabaseService.loadRepositoriesFromDatabase(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        } else if (snapshot.hasError) {
          return const Center(
            child: Text(
              'Error loading cached data',
              style: TextStyle(color: Colors.red), // Error message color
            ),
          );
        } else if (!snapshot.hasData || snapshot.data!.isEmpty) {
          return const Center(
            child: Text(
              'No cached data available',
              style: TextStyle(color: Colors.grey), // Message color
            ),
          );
        } else {
          final cachedRepositories = snapshot.data!;
          return ListView.builder(
            itemCount: cachedRepositories.length,
            itemBuilder: (context, index) {
              final repository = cachedRepositories[index];
              return RepositoryCard(repository: repository);
            },
          );
        }
      },
    );
  }
}

