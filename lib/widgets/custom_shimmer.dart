import 'package:flutter/material.dart';
import 'package:fluttter_github_repo/constants/colors.dart';
import 'package:shimmer/shimmer.dart';


class CustomShimmer extends StatelessWidget {
  const CustomShimmer({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      scrollDirection: Axis.vertical,
      padding: const EdgeInsets.symmetric(horizontal: 20),
      physics: const NeverScrollableScrollPhysics(),
      itemBuilder: (context, index) {
        return Shimmer.fromColors(
          baseColor: Colors.grey[300]!,
          highlightColor: AppColors.opacityColor,
          child: ListTile(
            title: Container(
              width: double.infinity,
              height: 20,
              color: Colors.white,
            ),
            subtitle: Container(
              width: double.infinity,
              height: 14,
              color: Colors.white,
            ),
          ),
        );
      },
    );
  }
}
