# Fluttter Github Repo

## The app is fully functional and works on real devices. I use the OOP structure in this app. For further scale, I will look for a state management solution like BLOC or Riverpod.

- Http request by dio and dio smart retry as interceptor.
- Pagination was implemented.
- Sqflite for local db caching.
- Internet connectivity checker 
- Shared preferences to persiist sorting value.
- Periodic api call after every 30 minutes.

![](./home.png)
![](./details.png)
 
